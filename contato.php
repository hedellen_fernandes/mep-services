<?php include "header.php";?>
<section id="banner" class="atendimento servicos">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-sm-12 col-xs-12">
				<h2>Pensando em suprir as necessidades de um mercado <br> cada vez mais exigente a MEP Service oferece as melhores soluções.</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7 col-sm-12 col-xs-12">
				<h1>NÃO DESPERDICE TEMPO E DINHEIRO, <br> CONTRATE CONFORME A SUA DEMANDA!</h1>
			</div>
		</div>
	</div>
</section>
<section id="corpo">
		<div class="container">
			<form action="mail.php" method="POST" id="contactForm">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12"><h5>DADOS PESSOAIS</h5></div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="text" name="nome" placeholder="NOME">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="email" name="email" placeholder="E-MAIL">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="tel" name="telefone" placeholder="TELEFONE" class="sp_celphones">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<span class="select">
							<select name="motivo">
								<option disabled selected>O QUE DESEJA</option>
								<option value="Elétrica">Elétrica</option>
								<option value="Seguranca">Segurança</option>
								<option value="Manutenção Predial">Manutenção Predial</option>
								<option value="Pintura">Pintura</option>								
								<option value="Ar Condicionado">Ar Condicionado</option>								
							</select>
						</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<h5>ENDEREÇO</h5>					
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<input type="text" name="logradouro" placeholder="LOGRADOURO">				
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="text" name="cep" placeholder="CEP" class="cep">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="text" name="bairro" placeholder="BAIRRO">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="text" name="cidade" placeholder="CIDADE">
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<input type="text" name="estado" placeholder="ESTADO">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea name="mensagem" placeholder="DIGITE AQUI SUA MENSAGEM"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-5 col-sm-12 col-xs 12">
						<p class="error">
					        <?php if(isset($_GET['msg'])):?>
			  					Sua mensagem foi enviada com sucesso!
					        <?php endif?>
					        <?php if(isset($_GET['err'])):?>
			  					Ops! Houve um erro, tente novamente!
					        <?php endif?>
						</p>
					</div>
					<div class="col-md-offset-5 col-md-2 col-sm-offset-2 col-sm-10 col-xs-12">
						<span class="submit">
							<input type="submit" class="btn btn-success" value="ENVIAR">
						</span>
					</div>
				</div>
			</form>
		</div>
	</section>
<?php include "footer.php";?>