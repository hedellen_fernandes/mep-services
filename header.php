<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>
		<?php
			$root = $_SERVER['DOCUMENT_ROOT'];
			$page = basename($_SERVER['SCRIPT_NAME']);
			function title_pagina(){
				$page = basename($_SERVER['SCRIPT_NAME']);
				if($page == "index.php") {echo "Início";}//INICIO
				if($page == "eletrica.php") {echo "Elétrica";}//ELETRICA
				if($page == "predial.php") {echo "Manutenção Predial";}//ELETRICA
				if($page == "pintura.php") {echo "Pintura";}//ELETRICA
				if($page == "ar.php") {echo "Ar Condicionado";}//ELETRICA
				if($page == "seguranca.php") {echo "Segurança";}//ELETRICA
				if($page == "contato.php") {echo "Atendimento";}//ELETRICA								


			}
			echo title_pagina(). " • MEPSERVICE";
		?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="robots" content="index, follow">
	<meta name="description" content="MEP Service, a empresa certa para Manutenção Predial da sua casa, sua empresa e seu condomínio!" />
	<meta name="keywords" content="MEP, manutenção, elétrica, hidráulica, telefonia, redes, predial, eletricista, pintor, jundiai, cftv, alarme, segurança, serviços, infraestrutura, engenharia, terceirização, climatização, câmera, condomínio, empresa, innera, praquemarido, incêndio, interfone,MEP SERVICE, Ar condicionado, manutenção predial, instalação elétrica, instalação hidráulica, cerca elétrica, campo limpo paulista, serviços terceirizados, instalação de ar condicionado, marido de aluguel, rede estruturada, soluções corporativas, faz tudo, painel elétrico, manutenção de condomínio, manutenção residencia"/>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="assets/component/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<script type="text/javascript" src="assets/component/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="assets/component/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/component/mask-plugin/dist/jquery.mask.min.js"></script>
	<script type="text/javascript" src="assets/component/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="assets/js/function.js"></script>

	<meta http-equiv= "Content-Language" content= "pt-br">

	<!-- <meta property="og:title" content="Maqdrau">
	<meta property="og:description" content="MAqdrau é uma empresa que oferece serviços e vendas de máquinas.">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Maqdrau">
	<meta property="og:url" content="http://maqdrau.com.br">
	<meta property="og:image" content="assets/img/icons/face-twitter.png">

	<meta name="twitter:card" content="MAqdrau é uma empresa que oferece serviços e vendas de máquinas.">
	<meta name="twitter:site" content="http://maqdrau.com.br">
	<meta name="twitter:creator" content="MAQDRAU">
	<meta name="twitter:title" content="Maqdrau">
	<meta name="twitter:description" content="MAqdrau é uma empresa que oferece serviços e vendas de máquinas.">
	<meta name="twitter:image" content="assets/img/icons/face-twitter.png">

	<link rel="apple-touch-icon" sizes="57x57" href="assets/img/icons/apple/57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/img/icons/apple/60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/img/icons/apple/72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/icons/apple/76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/img/icons/apple/114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/img/icons/apple/120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/img/icons/apple/144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/img/icons/apple/152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/icons/apple/180x180.png">

	<link rel="icon" type="image/png" href="assets/img/icons/android/32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="assets/img/icons/android/192x192.png" sizes="192x192">

	<link rel="icon" type="image/png" href="assets/img/icons/favicon114x114.png" sizes="96x96">
	<link rel="icon" type="image/png" href="assets/img/icons/favicon16x16.ico" sizes="16x16">		
	<link rel="shortcut icon" href="assets/img/icons/favicon16x16.ico">
	<meta name="msapplication-TileColor" content="#18bd5e">
	<meta name="msapplication-TileImage" content="assets/img/icons/windows10.png">
	<meta name="theme-color" content="#00052d">

	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="Maqdrau">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
 -->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navPrincipal" aria-expanded="false">
					<span class="sr-only">Botão de Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<p>MENU</p>
				</button>
				<a class="navbar-brand" href="index.php" title="Logo MEPSERVICES">
					<img src="assets/img/logo.svg" alt="Logo MEPSERVICES">
				</a>
			</div>
			<div class="collapse navbar-collapse" id="navPrincipal">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php" title="Início"<?php if ($page == 'index.php'){echo 'class="active"';}?>>INÍCIO</a></li>
					<li><a href="eletrica.php" title="Elétrica" <?php if ($page == 'eletrica.php'){echo 'class="active"';}?>>ELÉTRICA</a></li>
					<li><a href="seguranca.php" title="Segurança" <?php if ($page == 'seguranca.php'){echo 'class="active"';}?>>SEGURANÇA</a></li>
					<li><a href="predial.php" title="Manutenção Predial" <?php if ($page == 'predial.php'){echo 'class="active"';}?>>MANUTENÇÃO PREDIAL</a></li>
					<li><a href="pintura.php" title="Pintura" <?php if ($page == 'pintura.php'){echo 'class="active"';}?>>PINTURA</a></li>
					<li><a href="#" data-toggle="tooltip" data-placement="top" title="Em Breve" >AR CONDICIONADO</a></li>
					<li><a href="contato.php" title="Atendimento" class="no-padding <?php if ($page == 'contato.php'){echo 'active';}?>">ATENDIMENTO</a></li>
				</ul>
			</div>
		</div>
	</nav>







