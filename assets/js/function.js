var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};

$('.sp_celphones').mask(SPMaskBehavior, spOptions);
$('.cep').mask("00000-000");



$(document).ready( function() {
    $("#contactForm").validate({
        // Define as regras
        rules: {
            nome: {
                // campoNome será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            email: {
                // campoEmail será obrigatório (required) e precisará ser um e-mail válido (email)
                required: true
            },
            mensagem: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            telefone: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 10
            },
            logradouro: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            cep: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true, minlength: 8
            },
            bairro: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            cidade: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            },
            estado: {
                // campoMensagem será obrigatório (required) e terá tamanho mínimo (minLength)
                required: true
            }
        },
        // Define as mensagens de erro para cada regra
        messages: {
            nome: {
                required: "Digite Seu Nome."
            },
            email: {
                required: "Digite Seu E-mail.", email: "Digite um e-mail válido"
            },
            mensagem: {
                required: "Digite Sua Mensagem."
            },
            telefone: {
                required: "Digite Seu Telefone.", minLength: "Telefone Inválido"
            },
            logradouro: {
                required: "Digite Seu Logradouro."
            },
            cep: {
                required: "Digite Seu CEP.", minLength: "CEP Inválido"
            }, 
            bairro: {
                required: "Digite Seu Bairro."
            },
            cidade: {
                required: "Digite Sua Cidade."
            }, 
            estado: {
                required: "Digite Seu Estado."
            }          
        }
    });
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
