<?php include"header.php";?>
<section id="banner" class="pintura servicos">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-12 col-xs-12">
				<h1>SUPORTE RÁPIDO E EFICAZ!</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<h2>Comodidade e Qualidade de vida, do jeito que você quer!</h2>
			</div>
			<div class="col-md-offset-5 col-md-3 col-sm-12 col-xs-12">
				<a href="contato.php" class="btn btn-success" title="Faça um Orçamento">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="30px" height="30px" viewBox="528 333.033 864 864" enable-background="new 528 333.033 864 864" xml:space="preserve">
					<g>
						<path fill="#FFFFFF" d="M1232.052,333.033H687.948C599.982,333.033,528,399.885,528,481.601v566.879
							c0,81.715,71.982,148.554,159.948,148.554h544.104c87.966,0,159.948-66.839,159.948-148.554V481.601
							C1392,399.885,1320.018,333.033,1232.052,333.033z M1352,1048.479c0,61.425-53.811,111.416-119.947,111.416H687.948
							c-66.137,0-119.948-49.99-119.948-111.415V481.601c0-61.425,53.811-111.415,119.948-111.415h544.104
							c66.123,0,119.948,49.99,119.948,111.415L1352,1048.479L1352,1048.479z"/>
						<path fill="#FFFFFF" d="M1232.052,414.749H687.948c-39.69,0-71.995,30.011-71.995,66.852v116.586h688.081V481.601
							C1304.034,444.746,1271.742,414.749,1232.052,414.749z"/>
						<path fill="#FFFFFF" d="M615.966,896.172v152.307c0,36.841,32.305,66.852,71.995,66.852h248.036V896.172H615.966z
							 M874.977,1022.168h-80.163v78.394H757.19v-78.394h-80.204v-33.48h80.204v-77.706h37.625v77.706h80.163V1022.168z"/>
						<path fill="#FFFFFF" d="M983.963,642.75v472.581h248.09c39.676,0,71.982-30.01,71.982-66.852V642.75H983.963z M1242.974,932.501
							h-197.937v-33.129h197.937V932.501z M1242.974,858.737h-197.937v-33.169h197.937V858.737z"/>
						<path fill="#FFFFFF" d="M615.966,642.75v208.858h320.031V642.75H615.966z M826.215,765.168H725.735v-35.978h100.481V765.168z"/>
					</g>
					</svg>

					<span>SOLICITAR ORÇAMENTO</span>
				</a>
			</div>
		</div>
	</div>
</section>
<section id="desc" class="servicos">
	<div class="container pintura">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h3>PROFISSIONAIS COM REFERÊNCIA!</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<ul>
					<li>MANUTENÇÃO DE AR CONDICIONADO SPLIT:</li>
					<li>- Instalação elétrica do equipamento</li>
					<li>- Limpeza e Higienização</li>
					<li>- Manutenção corretiva e preventiva</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php include "footer.php"?>