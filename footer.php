<footer>
	<div class="container">
		<div class="row">
			<div class="topo">
				<div class="col-md-3 col-md-offset-1 col-sm-12 col-xs-12">
					<h4>REGIÕES ATENDIDAS</h4>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<p>São Paulo (Capital), Grande São Paulo, Regiões de Jundiaí e Campinas.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="corpo">
				<p><span>ATENDIMENTO</span> 11 4039 2126 / 11 9 4000 8408 / 11 9-9847-7181 / 11 9-9532-4502 / CONTATO@MEPSERVICE.COM.BR</p>
			</div>
		</div>
		<div class="row">
			<div class="direitos">
				<p>MEP SERVICE 2016 COPYRIGHT &COPY</p>
			</div>
		</div>
	</div>
</footer>
